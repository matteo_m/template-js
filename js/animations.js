//////////////////////////////////////////////////////////////////////////
//////// ANIMATIONS
////////////////////////////////////////////////////////////////////////
var anim = {
	init: function (){
		console.log("INIT anim");
	},
	set: function(){
		console.log("SET anim");
	},
	get: function(){
		console.log("GET anim");
		return {
		}
	},
	start: function (){
		console.log("START anim");

	},
	update: function (){
		console.log("UPDATE anim");
	
	},
	printStatus: function (){
		console.log("PRINT anim");
	
	},
	test: function(){
		this.init();
		this.set();
		this.get();
		this.start();
		this.update();
		this.printStatus();	
	}
}
