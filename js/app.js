/* Global Var */
window.onorientationchange=updateOrientation;

$(function(){
/*  
	start 	 = introduction
	rules 	 = explenations of the game
	game  	 = main status nothing happen
	question = show question and options
	answer   = show result
*/

var canvas = $("#canvas"); 
var path = "images/";


var init = {
	start: function (){
		checkbrowser(); 
		loadCssImages();
		system.test();
	}
}
//////////////////////////////////////////////////////////////////////////
//////// GAME
////////////////////////////////////////////////////////////////////////
var system = {
	init: function (){
		console.log("INIT system");
		nav.test();
		panel.test();
		interactions.test();
		anim.test();
	},
	set: function(){
		console.log("SET system");
	},
	get: function(){
		console.log("GET system");
		return {
		}
	},
	start: function (){
		console.log("START system");

	},
	update: function (){
		console.log("UPDATE system");
	
	},
	printStatus: function (){
		console.log("PRINT system");
	},
	test: function(){
		this.init();
		this.set();
		this.get();
		this.start();
		this.update();
		this.printStatus();	
	}
}
var interactions = {
	init: function (){
		console.log("INIT interaction");
	},
	set: function(){
		console.log("SET interaction");
	},
	get: function(){
		console.log("GET interaction");
		return {
		}
	},
	start: function (){
		console.log("START interaction");

	},
	update: function (){
		console.log("UPDATE interaction");
	
	},
	printStatus: function (){
		console.log("PRINT interaction");
	},
	addInteraction: function(){
		console.log("ADD interaction");
		$(canvas).live(event_down,function(e){
			e.stopPropagation();			
		});
	},
	removeInteraction: function(){
		console.log("REMOVE interaction");
		$(canvas).unbind(event_down);
	},
	test: function(){
		this.init();
		this.set();
		this.get();
		this.start();
		this.update();
		this.printStatus();	
	}
}

init.start();
});