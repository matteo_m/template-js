/////////////////////////////////////////////////////////////////////////////////////////////////
///// USEFUL FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////////////////////
function randomFromTo(from, to){
	return Math.floor(Math.random() * (to - from + 1) + from);
}
cloneArray = function(source) {
	var a = [];
    for (var i=0; i< source.length;i++) {
    	a.push(source[i]);
	}
	return a;
}
// PRELOAD IMAGES
/* ------------------------------------------------------------ */
function loadCssImages(){
	console.log($.preloadCssImages({statusTextEl: '#textStatus', statusBarEl: '#status'})); /* Preload images from css */
}

jQuery.preloadImages = function(arr)
{
  for(var i = 0; i<arr.length; i++)
  {
  	//console.log("PRELOADING:"+arr[i]);
    jQuery("<img>").attr("src", arr[i]);
  }
}
// CHECK BROWSER
/* ------------------------------------------------------------ */
function checkbrowser(){
	var ua = navigator.userAgent;
	if( ua.match(/iPhone/i) || ua.match(/iPad/i) || ua.match(/Android/i) ){
		// MOBILE DEVICE
		event_down =  "touchstart";
		event_move = "touchmove";
		event_release =  "touchend";

	}else{
		// PC BROWSER
		event_down = "mousedown";
		event_move = "mousemove";
		event_release = "mouseup";
	}
}
// PREVENT ZOOMING/DRAGGING ON MOBILE DEVICE
/* ------------------------------------------------------------ */
document.ontouchmove = function(e){
	
	e.preventDefault();
}
// CHECK ORIENTATION (CURRENTLY DORMANT)
/* ------------------------------------------------------------ */
function updateOrientation(){
	var orientation=window.orientation;
}