// ELEMENT LIST
/////////////////////////////////////////////
//  nav bar
//  card
//  panel
///////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//////// NAV
////////////////////////////////////////////////////////////////////////
var nav = {
	init: function (){
		console.log("INIT nav");
	},
	set: function(){
		console.log("SET nav");
	},
	get: function(){
		console.log("GET nav");
		return {
		}
	},
	start: function (){
		console.log("START nav");

	},
	update: function (){
		console.log("UPDATE nav");
	
	},
	printStatus: function (){
		console.log("PRINT nav");
	
	},
	test: function(){
		this.init();
		this.set();
		this.get();
		this.start();
		this.update();
		this.printStatus();	
	}
}
//////////////////////////////////////////////////////////////////////////
//////// CARD
////////////////////////////////////////////////////////////////////////
var panel = {
	init: function (){
		console.log("INIT panel");
	},
	set: function(){
		console.log("SET panel");
	},
	get: function(){
		console.log("GET panel");
		return {
		}
	},
	start: function (){
		console.log("START panel");

	},
	update: function (){
		console.log("UPDATE panel");
	
	},
	printStatus: function (){
		console.log("PRINT panel");
	
	},
	test: function(){
		this.init();
		this.set();
		this.get();
		this.start();
		this.update();
		this.printStatus();	
	}
}
